# Author: Joel Sunil
# Description: Multiples of 3 and 5
# Version: 21-09-17

sum1=0
for x in range(1, 999):
    if x % 3 == 0:
        sum1 = sum1 + x
    if x % 5 == 0:
        sum1 = sum1 + x

print(sum1)